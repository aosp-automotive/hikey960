#include <stdint.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <fcntl.h>
#include <malloc.h>
#include <pthread.h>
#include <hardware/lights.h>
#include <libusb/libusb.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <android/log.h>

static const int MAX_INTERRUPT_OUT_TRANSFER_SIZE = 0x2;

static const int VENDOR_ID = 0x16c0;
static const int PRODUCT_ID = 0x05dc;
static const int INTERFACE_NUMBER = 3; // 0-based interface index.

static const int INTERRUPT_OUT_ENDPOINT = 0x01+INTERFACE_NUMBER;
static const int TIMEOUT_MS = 5000;

static pthread_once_t g_init = PTHREAD_ONCE_INIT;
static pthread_mutex_t g_lock = PTHREAD_MUTEX_INITIALIZER;

static struct libusb_device_handle *devh = NULL;
static unsigned char lastHID = 0x20;

void init_globals(void){
	pthread_mutex_init(&g_lock, NULL);
}

void writehid(uint8_t value){

	int bytes_transferred;
	int result = 0;

	uint8_t data_out[MAX_INTERRUPT_OUT_TRANSFER_SIZE];

	// Store data in a buffer for sending.
	data_out[0] = 0x01;
	data_out[1] = value;

	// Write data to the device.
	result = libusb_interrupt_transfer(
			devh,
			INTERRUPT_OUT_ENDPOINT,
			data_out,
			MAX_INTERRUPT_OUT_TRANSFER_SIZE,
			&bytes_transferred,
			TIMEOUT_MS);

	if (result < 0){
		__android_log_print(ANDROID_LOG_DEBUG, "lights", "USB write error");
		libusb_release_interface(devh, 0);
		libusb_close(devh);
		libusb_exit(NULL);
		devh = NULL;
	}
}

void openhid(){
	int result = libusb_init(NULL);
	if (result >= 0){
		devh = libusb_open_device_with_vid_pid(NULL, VENDOR_ID, PRODUCT_ID);

		if (devh != NULL){
			// The HID has been detected.
			// Detach the hidusb driver from the HID to enable using libusb.
			libusb_detach_kernel_driver(devh, INTERFACE_NUMBER);
			result = libusb_claim_interface(devh, INTERFACE_NUMBER);
			if (result < 0){
				__android_log_print(ANDROID_LOG_DEBUG, "lights", "USB error claiming interface: %d", result);
				libusb_close(devh);
				libusb_exit(NULL);
				devh = NULL;
			}
		} else {
			__android_log_print(ANDROID_LOG_DEBUG, "lights", "USB open device error");
		}
	} else {
		__android_log_print(ANDROID_LOG_DEBUG, "lights", "USB libusb error: %d", result);
	}
}

static int rgb_to_brightness(struct light_state_t const* state){
	int color = state->color & 0x00ffffff;
	return ((77*((color>>16)&0x00ff))
		+ (150*((color>>8)&0x00ff)) + (29*(color&0x00ff))) >> 8;
}

static int set_light_backlight(struct light_device_t* dev,
			       struct light_state_t const* state){
	uint8_t brightness = (uint8_t) rgb_to_brightness(state);

	pthread_mutex_lock(&g_lock);
	if (brightness != lastHID){
		if (devh == NULL) openhid();
		if (devh != NULL) writehid(brightness);
		lastHID = brightness;
	}
	pthread_mutex_unlock(&g_lock);
	return 0;
}

static int close_lights(struct light_device_t *dev){
	if (dev) free(dev);
	return 0;
}

/** Open a new instance of a lights device using name */
static int open_lights(const struct hw_module_t* module, char const* name,
		       struct hw_device_t** device){
	int (*set_light)(struct light_device_t* dev,
			 struct light_state_t const* state);

	if (0 == strcmp(LIGHT_ID_BACKLIGHT, name))
		set_light = set_light_backlight;
	else
		return -EINVAL;

	pthread_once(&g_init, init_globals);

	struct light_device_t *dev = malloc(sizeof(struct light_device_t));
	memset(dev, 0, sizeof(*dev));

	dev->common.tag = HARDWARE_DEVICE_TAG;
	dev->common.version = 0;
	dev->common.module = (struct hw_module_t*)module;
	dev->common.close = (int (*)(struct hw_device_t*))close_lights;
	dev->set_light = set_light;

	*device = (struct hw_device_t*)dev;
	return 0;
}

static struct hw_module_methods_t lights_module_methods = {
	.open =  open_lights,
};

struct hw_module_t HAL_MODULE_INFO_SYM = {
	.tag = HARDWARE_MODULE_TAG,
	.version_major = 1,
	.version_minor = 0,
	.id = LIGHTS_HARDWARE_MODULE_ID,
	.name = "lights Module",
	.author = "Adam Serbinski",
	.methods = &lights_module_methods,
};
